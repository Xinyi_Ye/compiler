#include "source.h"
#include <iostream>
using namespace std;

// put some of the functions here to avoid declaration problem.


void UnaryAST::print(ASTPtr currloop)
{
    // in eeyone:
    // as left value: IdAST, Index (when index is LVal, the rightvalue can only be id/int)
    // as right value: int, funcCall, unaryOp, IdAST, Index    (UnaryOp, funcCall: print the computing and return a new id)
    // as part in expAST: int(return the string directly), (exp)(do the computing and return a new id), id(return the id directly).
    // as stmt: return xxx. (return + symbol/num) (print directly)
    // special treats: Index. as Left value: return directly. as right value / in exp: do the computing and return a new id.

    //cout<<"unary print\n";
    string tmpID;
    string out;
    if(is_const)
    {
        // simply return constValue and let its parent do the printing!
        myID = to_string(myValue);
    }
    else
    {
        //cout<<(int)type<<endl;
        switch (type)  // exp.nc; unaryast.nc; id.nc; indexast.nc; returnAST; funcCallAST;
        {
            // Int/id :return string directly
            // (exp), func call, unaryOp: do the computing and return the id
            // Return xxx: print the ast and print the id the ast returned.
            // Index: based on its use.
            case Type::IdAST:
                myID = ((IdAST*)ast)->myId();
                break;
            case Type::ExpBracket:
                ast->print(currloop);
                myID = ((ExpAST*)ast)->the_final_id_in_eeyone();
                break;
            case Type::UnaryOp:
                ast->print(currloop);
                tmpID = ((ExpAST*)ast)->the_final_id_in_eeyone();
                myID = newTmpID();
                out = myID;
                out += " = ";
                out += unary_ops[(int)op];
                out += tmpID;
                out += "\n";
                otherCode.emplace_back(out);
                break;
            case Type::funcCall:
                if(!func_is_void(((FuncCallAST*)ast)->id_in_sysY))
                {
                    myID = newTmpID();
                    ((FuncCallAST*)ast)->print_with_assign(currloop, myID);
                }
                else ((FuncCallAST*)ast)->print(currloop);

                break;
            case Type::returnAST:
                if(ast == nullptr)
                {
                    otherCode.emplace_back("return\n");
                }
                else
                {
                    ast->print(currloop);
                    out = "return ";
                    if(((ExpAST*)ast)->is_const)
                    {
                        tmpID = to_string(((ExpAST*)ast)->value());
                    }
                    else
                    {
                        tmpID = ((ExpAST*)ast)->the_final_id_in_eeyone();
                    }
                    out += tmpID;
                    out += "\n";
                    otherCode.emplace_back(out);
                }
                break;
            case Type::Index:
                // as Left value: return directly.
                // as right value / in exp: do the computing and return a new id.
                if(as_LVal)
                {
                    ast->print(currloop);  // print index_value ([xxx]) computing process!
                    myID = ((IndexAST*)ast)->myID();
                }
                else
                {
                    // Q: index as RVal should print itself in the indexAST node and return the id?
                    // A: no! if we want to access an index as exp, we must go through UnaryAST!

                    ast->print(currloop);  // print index_value ([xxx]) computing process!
                    myID = newTmpID();
                    out = myID;
                    out += " = ";
                    tmpID = ((IndexAST*)ast)->myID();
                    out += tmpID;
                    out += "\n";
                    otherCode.emplace_back(out);
                }
                break;
            default:
                assert("this cannot be non-const UnaryAST!" && 0);
        }
    }
    //cout<<"unary print end\n";
}

UnaryAST::UnaryAST(bool is_const, Operator op, ASTPtr ast,Type t, bool as_LVal):is_const(is_const),op(op),ast(ast),type(t),as_LVal(as_LVal)
{
    if(is_const)
    {
        // intAST; (exp).const;  UnaryOp UnaryAST.const; idAST.const; IndexAST.const;
        switch (type)
        {
            case Type::intAST:
                myValue = ((IntAST*)ast)->value();
                break;
            case Type::ExpBracket:
                myValue = ((ExpAST*)ast)->value();
                break;
            case Type::UnaryOp:
                switch(op)
                {
                    case Operator::Pos:
                        myValue = ((ExpAST*)ast)->value();break;
                    case Operator::Neg:
                        myValue = -(((ExpAST*)ast)->value());
                        break;
                    case Operator::Not:
                        myValue = !((ExpAST*)ast)->value();break;
                    default: assert("UnaryOp does not exist!" && false);//error
                }
                break;
            case Type::IdAST:
                myValue = ((IdAST*)ast)->value();
                break;
            case Type::Index:
                myValue = ((IndexAST*)ast)->value();
                // todo: when is index_value not const??? we need to make it clear when writing yacc!
                break;
            default: assert("is_const with non-const type!" && false);
        }
    }
    else  // exp.nc; unaryast.nc; id.nc; indexast.nc; returnAST; funcCallAST;
    {
        // do not need to do anything. myID is complicated. we do it in print() rather than here.
    }
}

string ExpAST::the_final_id_in_eeyone()  // meaningful only when not const
{
    assert(!is_const);
    //cout<<"is_binary: "<<is_binary<<endl;
    if(is_binary)return ((BinaryAST*)ast)->the_final_id_in_eeyone();
    else return ((UnaryAST*)ast)->the_final_id_in_eeyone();
}

int ExpAST::value()  // meaningful only when not const
{
    assert(is_const);
    if(is_binary)return ((BinaryAST*)ast)->value();
    else return ((UnaryAST*)ast)->value();
}

void BinaryAST::print(ASTPtr currloop)
{
    //cout<<"binary print\n";
    string out;
    if(is_const)
    {
        myID = to_string(myValue);
    }
    else
    {
        if(op != Operator::Assign)
        {
            left->print(currloop);
            right->print(currloop);

            string leftID,rightID;
            if(((ExpAST*)left)->is_const)leftID = to_string(((ExpAST*)left)->value());
            else leftID = ((ExpAST*)left)->the_final_id_in_eeyone();
            if(((ExpAST*)right)->is_const)rightID = to_string(((ExpAST*)right)->value());
            else rightID = ((ExpAST*)right)->the_final_id_in_eeyone();


            out = (myID);
            out+=(" = ");
            out+=(leftID);
            out+=(bin_ops[(int)op]);
            out+=(rightID);
            out+=("\n");
            otherCode.emplace_back(out);
            //cout<<leftID<<' '<<rightID<<endl;
        }
        else  // op == Operator::Assign
        {
            right->print(currloop);
            string rightID;
            if(((ExpAST*)right)->is_const) {rightID = to_string(((ExpAST*)right)->value());}
            else rightID = ((ExpAST*)right)->the_final_id_in_eeyone();
            // LVal must be UnaryAST.
            left->print(currloop);

            // if it is LVal: do not need to print.
            // call the function in order to calculate myID!
            string leftID = ((UnaryAST*)left)->the_final_id_in_eeyone();
            out=(leftID);
            out+=(" = ");
            out+=(rightID);
            out+=("\n");
            otherCode.emplace_back(out);
            //cout<<leftID<<' '<<rightID<<endl;
        }
    }
    //cout<<"binary print end\n";
}


void BinaryAST::print_logic(ASTPtr currloop, const string& then_stmt, const string& else_stmt)
{
    string out,tmp;
    assert(!is_const);
    assert(op != Operator::Assign);

    if(op != Operator::And && op!=Operator::Or)
    {
        left->print(currloop);
        right->print(currloop);

        string leftID,rightID;
        if(((ExpAST*)left)->is_const)leftID = to_string(((ExpAST*)left)->value());
        else leftID = ((ExpAST*)left)->the_final_id_in_eeyone();
        if(((ExpAST*)right)->is_const)rightID = to_string(((ExpAST*)right)->value());
        else rightID = ((ExpAST*)right)->the_final_id_in_eeyone();


        out = (myID);
        out+=(" = ");
        out+=(leftID);
        out+=(bin_ops[(int)op]);
        out+=(rightID);
        out+=("\n");
        otherCode.emplace_back(out);//cout<<leftID<<' '<<rightID<<endl;


        // print if-else.
        tmp = "if 0 != ";
        tmp += myID;
        tmp += " goto ";
        tmp += then_stmt;
        tmp += "\n";
        otherCode.emplace_back(tmp);
        tmp = "goto ";
        tmp += else_stmt;
        tmp += "\n";
        otherCode.emplace_back(tmp);
    }
    else if(op == Operator::And)
    {
        string test_right = newLabelId();
        left->print_logic(currloop,test_right,else_stmt);
        tmp = test_right;
        tmp += ":\n";
        otherCode.emplace_back(tmp);
        right->print_logic(currloop,then_stmt,else_stmt);
    }
    else  // op = Or
    {
        string test_right = newLabelId();
        left->print_logic(currloop,then_stmt,test_right);
        tmp = test_right;
        tmp += ":\n";
        otherCode.emplace_back(tmp);
        right->print_logic(currloop,then_stmt,else_stmt);
    }
}
void UnaryAST::print_logic(ASTPtr currloop, const string &then_stmt, const string &else_stmt)
{
    string tmpID;
    string out;
    assert(!is_const);

    //cout<<(int)type<<endl;
    switch (type)  // exp.nc; unaryast.nc; id.nc; indexast.nc; returnAST; funcCallAST;
    {
        // Int/id :return string directly
        // (exp), func call, unaryOp: do the computing and return the id
        // Return xxx: print the ast and print the id the ast returned.
        // Index: based on its use.
        case Type::IdAST:
            myID = ((IdAST*)ast)->myId();
            break;
        case Type::ExpBracket:
            ast->print(currloop);
            myID = ((ExpAST*)ast)->the_final_id_in_eeyone();
            break;
        case Type::UnaryOp:
            ast->print(currloop);
            tmpID = ((ExpAST*)ast)->the_final_id_in_eeyone();
            myID = newTmpID();
            out = myID;
            out += " = ";
            out += unary_ops[(int)op];
            out += tmpID;
            out += "\n";
            otherCode.emplace_back(out);
            break;

        case Type::Index:
            // as Left value: return directly.
            // as right value / in exp: do the computing and return a new id.
            if(as_LVal)
            {
                ast->print(currloop);  // print index_value ([xxx]) computing process!
                myID = ((IndexAST*)ast)->myID();
            }
            else
            {
                // Q: index as RVal should print itself in the indexAST node and return the id?
                // A: no! if we want to access an index as exp, we must go through UnaryAST!

                ast->print(currloop);  // print index_value ([xxx]) computing process!
                myID = newTmpID();
                out = myID;
                out += " = ";
                tmpID = ((IndexAST*)ast)->myID();
                out += tmpID;
                out += "\n";
                otherCode.emplace_back(out);
            }
            break;
        default:
            assert("this cannot be UnaryAST that has value!" && 0);
    }
    // print if-else.
    string tmp;
    tmp = "if 0 != ";
    tmp += myID;
    tmp += " goto ";
    tmp += then_stmt;
    tmp += "\n";
    otherCode.emplace_back(tmp);
    tmp = "goto ";
    tmp += else_stmt;
    tmp += "\n";
    otherCode.emplace_back(tmp);


}


VarDefAST::VarDefAST(bool is_const, ASTPtr idAST, ASTPtr expAST):is_const(is_const),idAST(idAST),expAST(expAST)
{
    if(is_const)
    {
        assert(expAST != nullptr);
        varList.emplace_back(varListElement(((IdAST*)idAST)->id_in_sysy(), is_const, ((ExpAST*)expAST)->value()));
    }
    else
    {
        varList.emplace_back(varListElement(((IdAST*)idAST)->id_in_sysy(), is_const, 0));
    }
}

void VarDefAST::print(ASTPtr currloop)
{
    //cout<<"var def print\n";
    // add to idlist
    if(is_const)
    {
        varList.emplace_back(varListElement(((IdAST*)idAST)->id_in_sysy(), is_const, ((ExpAST*)expAST)->value()));
    }
    else  // non-const
    {
        varList.emplace_back(varListElement(((IdAST*)idAST)->id_in_sysy(), is_const, 0));
    }


    // print def
    if(is_const)return;
    // not const
    string code;
    code = "var ";
    string id = ((IdAST*)idAST)->myId();
    code += id;
    code += "\n";
    varDefCode.emplace_back(code);
    if(expAST != nullptr)
    {
        expAST->print(currloop);

        code = id;
        code += " = ";
        //cout<<"here123\n";
        if(!(((ExpAST*)expAST)->is_const))
        {
            code += ((ExpAST*)expAST)->the_final_id_in_eeyone();
        }
        else
        {
            code += to_string(((ExpAST*)expAST)->value());
        }
        code += "\n";
        otherCode.emplace_back(code);
    }
    //cout<<"var def print end\n";
}


ArrayDefAST::ArrayDefAST(bool is_const, ASTPtr idAST, ASTPtr initlistAST, const vector<ASTPtr>& dim ) : is_const(is_const), idAST(idAST), initlistAST(initlistAST)
{
    for(int i=0;i<dim.size();i++)
    {
        assert(((ExpAST*)dim[i])->is_const);  // inside[]: must be constexpr!
        dims.emplace_back(((ExpAST*)dim[i])->value());
    }


    arrayListElement *tmp;
    int tmpint;
    int sum_dim=1;
    if(is_const)
    {
        assert(initlistAST != nullptr);
        tmp=new arrayListElement(((IdAST*)idAST)->id_in_SysY, is_const, dims);


        tmpint = ((InitListAST*)initlistAST)->init_value(0,dims,tmp->value,0);  // return value: the current index

        // assert that tmp int = sum_dim (the init process is completed)
        for(int i=0;i<dims.size();i++)
        {
            sum_dim *= dims[i];
        }
        assert(tmpint == sum_dim);  // the size must>0 for the 0th layer!

        arrayList.emplace_back(*tmp);

    }
    else
    {
        arrayList.emplace_back(arrayListElement(((IdAST*)idAST)->id_in_SysY, is_const, dims));
        // the value calculation is done when print !
    }
}

void ArrayDefAST::print(ASTPtr currloop)
{
    //cout<<"get array def print\n";
    arrayListElement *tmpa;
    int tmpint;

    int sum_dim=1;
    for(int i=0;i<dims.size();i++)
    {
        sum_dim *= dims[i];
    }

    // add id to idlist
    if(is_const)
    {
        //cout<<"here 1\n";
        assert(initlistAST != nullptr);
        tmpa = new arrayListElement(((IdAST*)idAST)->id_in_SysY, is_const, dims);


        tmpint = ((InitListAST*)initlistAST)->init_value(0,dims,tmpa->value,0);  // return value: the current index


        // assert that tmp int = sum_dim (the init process is completed)
        assert(tmpint == sum_dim);  // the size must>0 for the 0th layer!

        arrayList.emplace_back(*tmpa);
    }
    else
    {
        //cout<<"here 2\n";
        arrayList.emplace_back(arrayListElement(((IdAST*)idAST)->id_in_SysY, is_const, dims));
        // the value calculation is done when print !
    }

    // here: we should print the array no matter it is const or not.
    // for const int a [x][y]: we might use a[xxx][yyy ( non-const ) ]!

    // print " var sum_dim Tx"

    string tmp = "var ";
    string indexID = ((IdAST*)idAST)->myId();

    //cout<<indexID<<endl;  // yxy: this is where the bug is !
    tmp += to_string(sum_dim * 4);
    tmp += " ";
    tmp += indexID;
    tmp += "\n";
    varDefCode.emplace_back(tmp);

    //cout<<tmp;

    if(is_const)
    {
        // print its initial value

        for(int i = arrayList.size()-1;i>=0;i--)
        {
            if(arrayList[i].id_in_SysY == indexID)
            {
                tmpa = &(arrayList[i]);
                break;
            }
        }
        vector<int>& value_list = tmpa->value;
        for(int i=0;i<value_list.size();i++)
        {
            tmp = indexID;
            tmp += "[";
            tmp += to_string(i*4);
            tmp += "] = ";
            tmp += to_string(value_list[i]);
            tmp += "\n";
            otherCode.emplace_back(tmp);
        }
    }
    else if(initlistAST != nullptr)  // print initialization(non-const)
    {
        int tmpIndex = ((InitListAST*)initlistAST)->print_value(0,dims,currloop,indexID,0);
        assert(tmpIndex == sum_dim);
    }
    //cout<<"getout array def print\n";
}

InitListAST::InitListAST(const vector<InitListAST*>& things, ASTPtr expAST):expAST(expAST)
{
    for(int i=0;i<things.size();i++)
    {
        things_in_brace.emplace_back(things[i]);
    }
}

int InitListAST::init_value(int currIndex, const vector<int>& dim, vector<int>& value, int layer)  // the array is const!
{
    ExpAST* expastPtr=(ExpAST*)expAST;
    int sum_dim=1;
    if(things_in_brace.empty())
    {
        if(expastPtr == nullptr)  // empty
        {
            return currIndex;  // add nothing to value
        }
        else  // expAST
        {
            assert(expastPtr->is_const);
            //cout<<expastPtr->value()<<endl;
            value.emplace_back(expastPtr->value());
            return currIndex+1;
        }
    }
    else
    {
        for(int i = 0; i < things_in_brace.size(); i++)
        {
            currIndex = things_in_brace[i]->init_value(currIndex, dim, value,layer + 1);
        }

        // calculate sum_dim
        for(int i=layer;i<dim.size();i++)
        {
            sum_dim *= dim[i];
        }

        for(int i = currIndex;i<sum_dim;i++)  // init implicitly with 0
        {
            currIndex++;
            value.emplace_back(0);
        }

        return currIndex;
    }
}

int InitListAST::print_value(int currIndex, const vector<int>& dim, ASTPtr currloop, const string& ArrayId,int layer)  // the array is not const!
{
    ExpAST* expastPtr=(ExpAST*)expAST;
    int sum_dim=1;
    string tmp;
    string codetmp;
    int now_num=0;
    if(things_in_brace.empty())
    {
        if(expastPtr == nullptr)  // empty
        {
            // calculate sum_dim
            for(int i=layer;i<dim.size();i++)
            {
                sum_dim *= dim[i];
            }

            for(int i = now_num;i < sum_dim;i++)  // init implicitly with 0
            {
                codetmp = ArrayId;
                codetmp += "[";
                codetmp += to_string(currIndex*4);
                codetmp += "] = 0\n";
                otherCode.emplace_back(codetmp);

                now_num++;
                currIndex++;
            }
            return currIndex;  // add nothing to value
        }
        else  // expAST
        {
            //assert(! (expastPtr->is_const));
            expastPtr->print(currloop);
            if(expastPtr->is_const)tmp = to_string(expastPtr->value());
            else tmp = expastPtr->the_final_id_in_eeyone();
            codetmp = ArrayId;
            codetmp += "[";
            codetmp += to_string(currIndex*4);
            codetmp += "] = ";
            codetmp += tmp;
            codetmp += "\n";
            otherCode.emplace_back(codetmp);
            return currIndex + 1;
        }
    }
    else
    {
        int tempp = currIndex;
        for(int i = 0; i < things_in_brace.size(); i++)
        {
            currIndex = things_in_brace[i]->print_value(currIndex, dim, currloop, ArrayId,layer+1);
        }
        now_num = currIndex - tempp;

        // calculate sum_dim
        for(int i=layer;i<dim.size();i++)
        {
            sum_dim *= dim[i];
        }
        //cout<<layer<<' '<<sum_dim<<endl;
        for(int i = now_num;i < sum_dim;i++)  // init implicitly with 0
        {
            codetmp = ArrayId;
            codetmp += "[";
            codetmp += to_string(currIndex*4);
            codetmp += "] = 0\n";
            otherCode.emplace_back(codetmp);

            now_num++;
            currIndex++;
        }
        return currIndex;
    }
}

void FuncCallAST::print(ASTPtr currloop)
{
    print_params(currloop);

    //call f_xxx
    string code = "call f_";
    code += id_in_sysY;
    code += "\n";
    otherCode.emplace_back(code);
}

void FuncCallAST::print_with_assign(ASTPtr currloop, string LVal)
{
    // output params
    print_params(currloop);

    // Lval = call xxx
    string code = LVal;
    code += " = call f_";
    code += id_in_sysY;
    code += "\n";
    otherCode.emplace_back(code);

}

void FuncCallAST::print_params(ASTPtr currloop)
{
    //params: calculate the param (symbol/num only in eeyone) first, then "param tx"
    // params:
    // type 1: id  / index_id(array pt)
    //          -- add idAST into params. if id.find_info==nullptr, this should be index_id. we may search it in arraylist, and return its id_in_ir.
    //          -- if id: if const, use its value!
    // type 2: id[a]  (array pt)
    //          -- looks like indexAST, so we use index.find_info to search it. if info().dim.size > dim.size, handle it as pt.
    // type 3: id[a] (array element)
    //          -- looks like indexAST, so we use index.find_info to search it. output "t1 = id[a]"(use index.print() to calculate a and use myID() to output "id[a]") and use t1 as the param
    // type 4: exp
    //          -- print it and use the final_id to be the param.
    //          -- if const, use its value!
    string code;
    string tmp;
    for(int i=0;i<params.size();i++)
    {
        if(params_type[i]==Type::exp)
        {
            if(((ExpAST*)params[i])->is_const)
            {
                code = "param ";
                code += to_string(((ExpAST*)params[i])->value());
                code += "\n";
                otherCode.emplace_back(code);
            }
            else  // is not const
            {
                ((ExpAST*)params[i])->print(currloop);
                code = "param ";
                code += ((ExpAST*)params[i])->the_final_id_in_eeyone();
                code += "\n";
                otherCode.emplace_back(code);
            }
        }
        else if (params_type[i]==Type::index)
        {
            arrayListElement* info = ((IndexAST*)params[i])->find_info();
            if(info->dims.size() == ((IndexAST*)params[i])->dims.size())  // it is a int. (array element)
            {
                // t1 = id[a];
                // param t1;

                ((IndexAST*)params[i])->print(currloop);  // print a calculation
                tmp = newTmpID();
                code = tmp;
                code += " = ";
                code += ((IndexAST*)params[i])->myID();
                code += "\n";
                otherCode.emplace_back(code);

                code = "param ";
                code += tmp;
                code += "\n";
                otherCode.emplace_back(code);

            }
            else  // a sub-array
            {

                int len_array=1;  // if a[5][4][3][2]  -->  a[2][3]: len_array = 6!
                assert(!(((IndexAST*)params[i])->dims.empty()));  // for funcparams: we want info->dims[0] to be meaningless!
                for(int j = ((IndexAST*)params[i])->dims.size(); j < info->dims.size();j++)
                {
                    len_array *= info->dims[j];
                }
                vector<ExpAST*>& dim = ((IndexAST*)params[i])->dims;
                string index_number = newTmpID();  // tb
                // tb = 0
                code = index_number;
                code += " = 0\n";
                otherCode.emplace_back(code);

                string temp=newTmpID();  // ta

                for(int j = dim.size()-1;j>=0;j--)
                {
                    if(dim[j]->is_const)
                    {
                        int int_tmp=dim[j]->value() * len_array;
                        // tb = tb + int_tmp
                        code = index_number;
                        code += " = ";
                        code += index_number;
                        code += " + ";
                        code += to_string(int_tmp);
                        code += "\n";
                        otherCode.emplace_back(code);
                    }
                    else  // not const
                    {
                        dim[j]->print(currloop);
                        string tmpId = dim[j]->the_final_id_in_eeyone();

                        // ta = id * len_array
                        code = temp;
                        code += " = ";
                        code += tmpId;
                        code += " * ";
                        code += to_string(len_array);
                        code += "\n";
                        otherCode.emplace_back(code);

                        // tb = tb + ta;
                        code = index_number;
                        code += " = ";
                        code += index_number;
                        code += " + ";
                        code += temp;
                        code += "\n";
                        otherCode.emplace_back(code);
                    }
                    len_array *= info->dims[j];
                }
                // tb = tb * 4
                code = index_number;
                code += " = ";
                code += index_number;
                code += " * 4\n";
                otherCode.emplace_back(code);

                // tc = Ta(index id) + tb
                tmp = newTmpID(); // tc
                code = tmp;
                code += " = ";
                code += ((IndexAST*)params[i])->my_ident_only();
                code += " + ";
                code += index_number;
                code += "\n";
                otherCode.emplace_back(code);

                // param tc
                code = "param ";
                code += tmp;
                code += "\n";
                otherCode.emplace_back(code);

            }
        }
        else  // looks like id
        {
            pair<void*,bool> information = ((IdAST*)params[i])->find_info();

            if(information.second)  // index id
            {
                for(int j = arrayList.size() - 1; j >= 0; j --)
                {
                    if(arrayList[j].id_in_SysY == ((IdAST*)params[i])->id_in_SysY)
                    {
                        code = "param ";
                        code += arrayList[j].id_in_ir;
                        code += "\n";
                        otherCode.emplace_back(code);
                        break;
                    }
                }
            }
            else  // real id
            {
                varListElement* infor = (varListElement*)information.first;
                if(infor->is_const)
                {
                    code = "param ";
                    code += to_string(infor->value);
                    code += "\n";
                    otherCode.emplace_back(code);
                }
                else
                {
                    code = "param ";
                    code += infor->id_in_ir;
                    code += "\n";
                    otherCode.emplace_back(code);
                }
            }
        }
    }
}

extern int tmp_id_cnt;
void FuncDefAST::print(ASTPtr currloop)
{
    funcname_isvoid.emplace_back(pair<string,bool>(((IdAST*)idAst)->id_in_sysy(),!if_int));

    //print global vars
    for(int i = 0;i < varDefCode.size();i++)
    {
        cout<<varDefCode[i];
    }
    varDefCode.clear();

    string tmp = "f_";
    tmp += ((IdAST*)idAst)->id_in_sysy();
    tmp += " [";
    tmp += to_string(params.size());
    tmp += "]\n";
    cout<<tmp;
    //otherCode.emplace_back(tmp);



    pushBlockStart();

    paramcnt=0;
    for(int i=0;i<params.size();i++)
    {
        params[i]->print(currloop);  // will not print. just push var into varlist!
    }

    block->print(currloop);

    if(!if_int)
    {
        tmp = "return\n";
        otherCode.emplace_back(tmp);
    }

    popBlockStart_and_pop_elements();

    tmp = "end f_";
    tmp += ((IdAST*)idAst)->id_in_sysy();
    tmp += "\n";
    otherCode.emplace_back(tmp);

    // output all codes!
    for(int i=0;i<tmp_id_cnt;i++)
    {
        string ans = "t";
        ans += to_string(i);
        print_var_def_code(ans);
    }


    for(int i = 0;i < varDefCode.size();i++)
    {
        cout<<varDefCode[i];
    }



    for(int i = 0;i < otherCode.size();i++)
    {
        cout<<otherCode[i];
    }
    varDefCode.clear();
    otherCode.clear();
}

FuncParamAST::FuncParamAST(bool is_array, ASTPtr idAST, const vector<ASTPtr>& dim):is_array(is_array),idAST(idAST)
{
    if(is_array)
    {
        // init dims
        dims.emplace_back(0);  // dims[0] = 0.
        for(int i = 0;i < dim.size();i++)
        {
            assert(((ExpAST*)dim[i])->is_const);  // inside[]: must be constexpr!
            dims.emplace_back(((ExpAST*)dim[i])->value());
        }

        assert(dim.size() == dims.size() - 1);
        arrayList.emplace_back(arrayListElement(((IdAST*)idAST)->id_in_SysY, false, dims,true));
    }
    else  // is id. not array
    {
        varList.emplace_back(varListElement(((IdAST*)idAST)->id_in_sysy(), false, 0, true));
    }
}
void FuncParamAST::print(ASTPtr currloop)  // no need to print it. adding params to idlist is enough.
{
    if(is_array)
    {
        arrayList.emplace_back(arrayListElement(((IdAST*)idAST)->id_in_SysY, false, dims,true));
    }
    else  // is id. not array
    {
        varList.emplace_back(varListElement(((IdAST*)idAST)->id_in_sysy(), false, 0, true));
    }
}

void IfElseAST::print(ASTPtr currloop)
{
    // jmp

    string tmp;
    if(((ExpAST*)cond)->is_const)  // unconditioned jmp
    {
        cond->print(currloop);
        if(((ExpAST*)cond)->value() != 0)
        {
            // then stmt only
            stmt_then->print(currloop);
        }
        else  // else stmt only
        {
            if(stmt_else != nullptr)
            {
                stmt_else->print(currloop);
            }
        }
    }
    else  // conditioned jmp
    {
        string label1 = newLabelId(), label2 = newLabelId();
        string label_else = newLabelId();
        //jmp

        cond->print_logic(currloop,label1,label_else);


        // else stmt
        otherCode.emplace_back(label_else + ":\n");
        if(stmt_else != nullptr)
        {
            stmt_else->print(currloop);
        }
        tmp = "goto ";
        tmp += label2+ "\n";
        otherCode.emplace_back(tmp);

        // then stmt
        otherCode.emplace_back(label1+ ":\n");
        stmt_then->print(currloop);

        otherCode.emplace_back(label2+ ":\n");
    }
}

void WhileAST::print(ASTPtr currloop)
{
    startLabel = newLabelId();
    endLabel = newLabelId();  // else label
    otherCode.emplace_back(startLabel+ ":\n");

    string then_label = newLabelId();
    // jmp

    string tmp;
    if(((ExpAST*)cond)->is_const)  // unconditioned jmp
    {
        cond->print(currloop);
        if(((ExpAST*)cond)->value() != 0)
        {
            // no need to print label and jmp code
        }
        else  // this code should not exist
        {
            return;
        }
    }
    else  // conditioned jmp
    {
        //jmp
        cond->print_logic(currloop,then_label,endLabel);
    }
    //stmt
    otherCode.emplace_back(then_label + ":\n");
    stmt->print(this);

    tmp = "goto ";
    tmp += startLabel + "\n";
    otherCode.emplace_back(tmp);

    otherCode.emplace_back(endLabel + ":\n");
}
