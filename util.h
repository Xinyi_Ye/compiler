#include <iostream>
#include <utility>
#include <vector>
#include <string>
using namespace std;


inline int oct2dec(const string& oct/* may begin with more than one 0 */)
{
    int ans=0;
    for(int i=1;i<oct.length();i++)
    {
        if(oct[i]=='\n')break;
        ans *= 8;
        ans += oct[i]-'0';
    }
    return ans;
}
inline int hex2dec(const string& hex/* begin with 0x */)
{
    int ans=0;
    for(int i=2;i<hex.length();i++)
    {
        if(hex[i]=='\n')break;
        ans *= 16;
        if(hex[i]>='0' && hex[i]<='9')  // 0-9
        {
            ans += hex[i]-'0';
        }
        else if(hex[i]>='A' && hex[i]<='F')  //A-F
        {
            ans += hex[i]-'A'+10;
        }
        else // a-f
        {
            ans += hex[i]-'a'+10;
        }
    }
    return ans;
    //return 0;
}

extern vector<string> varDefCode;

inline void print_var_def_code(const string& id)
{
    string code;
    code = "var ";
    code += id;
    code += "\n";
    varDefCode.emplace_back(code);
}



extern int tmp_id_cnt;
inline string newTmpID()
{
    //todo: vardefCode: for tmp id: did not print its vardef! it should be done here!
    string ans = "t";
    ans+=to_string(tmp_id_cnt);
    tmp_id_cnt++;
    //print_var_def_code(ans);
    return ans;
}

extern int IRIDcnt;
inline string newIRID()
{
    //todo: vardefCode is done when print vardef ast. no need to print it here.
    string ans = "T";
    ans+=to_string(IRIDcnt);
    IRIDcnt++;
    return ans;
}
extern int paramcnt;
inline string newParamId()
{
    string ans = "p";
    ans+=to_string(paramcnt);
    paramcnt++;
    return ans;
}
extern int labelcnt;
inline string newLabelId()
{
    string ans = "l";
    ans+=to_string(labelcnt);
    labelcnt++;
    return ans;
}



extern string bin_ops[14];

extern string unary_ops[3];  // "+" can be omitted.






struct varListElement
{
    string id_in_SysY;
    string id_in_ir;
    bool is_const;
    int value;  // only meaningful if is_const
    bool is_param;
    varListElement(string id_in_SysY, bool is_const, int value, bool is_param = false): id_in_SysY(std::move(id_in_SysY)), is_const(is_const), value(value), is_param(is_param)
    {
        if(is_param) id_in_ir = newParamId();
        else id_in_ir = newIRID();
    }
};

struct arrayListElement
{
    string id_in_SysY;
    string id_in_ir;
    bool is_const;
    vector<int> value;  // only meaningful if is_const
    vector<int> dims;
    bool is_param;

    arrayListElement(string id_in_SysY, bool is_const, vector<int>& dim, bool is_param = false): id_in_SysY(std::move(id_in_SysY)), is_const(is_const), is_param(is_param)
    {
        // do not init value here. do it in the arraydefAST class.
        if(is_param) id_in_ir = newParamId();
        else id_in_ir = newIRID();

        for(int i=0; i<dim.size();i++)
        {
            dims.push_back(dim[i]);
        }
    }
};

extern vector<varListElement> varList;
extern vector<int> varBlockStart;
extern vector<arrayListElement> arrayList;
extern vector<int> arrayBlockStart;

extern vector<pair<string,bool>> funcname_isvoid;
inline bool func_is_void(const string& funcName)
{
    for(int i = 0;i<funcname_isvoid.size();i++)
    {
        if(funcname_isvoid[i].first == funcName)return funcname_isvoid[i].second;
    }

    if(funcName == "getint"||funcName == "getch"||funcName == "getarray")return 0;
    return 1;
}







inline void pushBlockStart()// todo: in yacc: these works are done before the first Vn being reduced.
{
    //cout<<varList.size()<<" pushBlockStart\n";//debug
    varBlockStart.emplace_back(varList.size());  // varList.size() is the place of the first element!
    arrayBlockStart.emplace_back(arrayList.size());
}

inline void popBlockStart_and_pop_elements()  // todo: in yacc: these works are done after the whole blockAST is constructed
{
    if(varBlockStart.empty())// global variables
    {
        //cout<<" popBlockStart-global\n";//debug
        varList.clear();
        arrayList.clear();
        return;
    }
    // local variables

    int varlist_start = varBlockStart[varBlockStart.size()-1];
    //cout<<varlist_start<<' '<<varList.size()<<" popBlockStart-local\n";//debug
    for(int i = varlist_start;varList.size()>i;)
    {
        varList.pop_back();
        //cout<<"here\n";
    }
    //cout<<' '<<varList.size()<<" popBlockStart-local\n";//debug

    int arraylist_start = arrayBlockStart[arrayBlockStart.size()-1];
    for(int i = arraylist_start;i<arrayList.size();)
    {
        arrayList.pop_back();
    }

    varBlockStart.pop_back(); // varList.size() is the place of the first element!
    arrayBlockStart.pop_back();
}
