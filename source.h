#include <iostream>
#include <vector>
#include "util.h"
#include <map>
#include <cassert>
using namespace std;

// the ast classes reference(yxy edits them): MimiC. https://github.com/ustb-owl/MimiC/blob/master/src/define/ast.h#L87

extern vector<varListElement> varList;
extern vector<int> varBlockStart;
extern vector<arrayListElement> arrayList;
extern vector<int> arrayBlockStart;

extern vector<pair<string,bool>> funcname_isvoid;

extern vector<string> varDefCode;
extern vector<string> otherCode;


class BaseAST {
public:
    virtual ~BaseAST() = default;
    virtual void print(BaseAST* currLoop)=0;
    virtual void print_logic(BaseAST* currloop,const string& then_stmt, const string& else_stmt){};
};

using ASTPtr = BaseAST*;
using ASTPtrList = vector<ASTPtr>;

// root of the tree
class CompUnit : public BaseAST
{
public:
    ASTPtr CompUnitAST, AST;  // AST may be decl or funcdef
    CompUnit(ASTPtr a, ASTPtr b):CompUnitAST(a),AST(b){}
    void print(ASTPtr currLoop)override
    {
        //cout<<"get comp unit print\n";
        CompUnitAST->print(currLoop);
        AST->print(currLoop);
        //cout<<"getout comp unit print\n";
    }
};


// variable/constant declaration
class VarDeclAST : public BaseAST {
public:
    bool is_const;
    ASTPtrList varDeclASTs;  // actually it should be named vardefASTs.

    explicit VarDeclAST(bool const_or_not,const ASTPtrList& varDeclASTs):is_const(const_or_not),varDeclASTs(varDeclASTs){}
    void print(ASTPtr currLoop)override
    {
        //cout<<"get var decl print\n";
        if(is_const)return;
        for(auto & varDeclAST : varDeclASTs)
        {
            varDeclAST->print(currLoop);
        }
        //cout<<"getout var decl print\n";
    }

};

// variable/constant definition
class VarDefAST : public BaseAST  //add \n at the end
{
public:
    bool is_const;
    ASTPtr idAST, expAST;

    // when construct this node: need to put all the info into ast!
    // new idlist element here! indexAST and the idAST are mainly used to access the info in idlist!
    VarDefAST(bool is_const, ASTPtr idAST, ASTPtr expAST);

    void print(ASTPtr currloop)override;
};

// initializer list (for array initialization)
class ArrayDefAST : public BaseAST {
public:
    bool is_const;
    ASTPtr idAST, initlistAST;
    vector<int> dims;

    // when construct this node: need to put all the info into ast!
    // new idlist element here! indexAST and the idAST are mainly used to access the info in idlist!

    ArrayDefAST(bool is_const, ASTPtr idAST, ASTPtr initlistAST, const vector<ASTPtr>& dim);
    void print(ASTPtr currloop)override;


};

class InitListAST : public BaseAST{  // need to do some recursion in it.
public:
    vector<InitListAST*> things_in_brace;  // { , , , }
    ASTPtr expAST;
    // if things_in_brace.size()==0 && expAST==nullptr, this initlistAst is empty(situations like "{}" ).
    //int layer;  // useful when things_in_brace.size() != 0

    InitListAST(const vector<InitListAST*>& things, ASTPtr expAST);  // where can we get layer info ? Vn initlist's attribute is layer?

    int init_value(int currIndex, const vector<int>& dim,vector<int>& value,int layer);  // only used by const.
    // if not const, expAST may be calculated(print the computing process)when print(currloop).

    int print_value(int currIndex, const vector<int>& dim,ASTPtr currloop, const string& ArrayId,int layer);  // only used by non-const.
    void print(ASTPtr currloop)override{}  // no need to implement it. neverused!
};


// function definition
class FuncDefAST : public BaseAST {
public:
    bool if_int;  // 1:int; 0:void
    ASTPtr idAst,block;
    vector<ASTPtr> params;
    FuncDefAST(bool if_int,ASTPtr idAST,const vector<ASTPtr>& params,ASTPtr blockAst):if_int(if_int),idAst(idAST),params(params),block(blockAst){}
    void print(ASTPtr currloop)override;

};

// function parameter
// NOTE: if parameter is an array, 'arr_lens_' must not be empty
//       but its first element is 'nullptr' (e.g. int arg[])
class FuncParamAST : public BaseAST
{
    // if array: initialing it takes effort!
public:
    bool is_array;
    ASTPtr idAST;
    vector<int> dims;  // dims.size() is the real dim! dims[0] = 0! not important!.

    FuncParamAST(bool is_array, ASTPtr idAST, const vector<ASTPtr>& dim);
    void print(ASTPtr currloop)override;  // no need to print it. adding params to idlist is enough.

};

// function call
class FuncCallAST : public BaseAST {  // IDENT "(" [FuncRParams] ")"
public:
    string id_in_sysY;  // function name
    ASTPtrList params;  // ( expAST, indexAST, idAST. handle them differently! )
    enum class Type{ exp, index, id};
    vector<Type> params_type;


    explicit FuncCallAST(string& id):id_in_sysY(id){}
    void add_param(ASTPtr param, int type)
    {
        params.emplace_back(param);
        params_type.emplace_back((Type)type);
    }

    void print(ASTPtr currloop)override;

    void print_params(ASTPtr currloop);

    void print_with_assign(ASTPtr currloop, string LVal);

};


// statement block
class BlockAST : public BaseAST {
public:
    vector<ASTPtr> stmts_or_decls;

    BlockAST(const vector<ASTPtr>& stmts_or_decls):stmts_or_decls(stmts_or_decls){}
    void print(ASTPtr currloop)override
    {
        pushBlockStart();

        for(int i = 0; i < stmts_or_decls.size(); i ++)
        {
            if(stmts_or_decls[i] != nullptr)stmts_or_decls[i]->print(currloop);
            // there may be stmts like ";" .
        }
        popBlockStart_and_pop_elements();//在print的最后删掉所有的idlist！
    }
};

// if-else statement
class IfElseAST : public BaseAST {
public:
    ASTPtr cond;  // expast
    ASTPtr stmt_then,stmt_else;  // stmt_else may be nullptr
    IfElseAST(ASTPtr cond, ASTPtr stmt_then,ASTPtr stmt_else=nullptr):cond(cond),stmt_then(stmt_then),stmt_else(stmt_else){}
    void print(ASTPtr currloop)override;
};

// while statement
class WhileAST : public BaseAST
{
public:
    ASTPtr cond;  // expast
    ASTPtr stmt;
    string startLabel, endLabel;  // initialized in print()
    WhileAST(ASTPtr cond, ASTPtr stmt):cond(cond),stmt(stmt){}

    void print(ASTPtr currloop)override;
};

// control statement
class ControlAST : public BaseAST {  // break | continue
public:
    bool is_continue;  // 1: continue; 0: break;
    ControlAST(bool is_continue):is_continue(is_continue){}
    void print(ASTPtr currloop)override
    {
        WhileAST* currLoop = (WhileAST*)currloop;
        string tmp = "goto ";
        if(is_continue)
        {
            tmp += currLoop->startLabel;
            tmp += "\n";
            otherCode.emplace_back(tmp);
            return;
        }
        else  // break
        {
            tmp += currLoop->endLabel;
            tmp += "\n";
            otherCode.emplace_back(tmp);
            return;
        }
    }
};










class ExpAST : public BaseAST{  // BinaryAST | UnaryAST . value = their value; (if not const: no need to compute value!!!) const or not = their const or not.
public:
    bool is_const;
    bool is_binary;
    ASTPtr ast;  // either binary or unary ast.

    // the 2 following func: defined after class binaryAST and unaryAST.
    string the_final_id_in_eeyone();  // meaningful only when not const
    int value();

    ExpAST(bool is_const, bool is_binary, ASTPtr ast):is_const(is_const), is_binary(is_binary), ast(ast){}
    void print(ASTPtr currloop)override
    {
        ast->print(currloop);
    }
    void print_logic(ASTPtr currloop,const string& then_stmt, const string& else_stmt)
    {
        ast->print_logic(currloop,then_stmt,else_stmt);
    }
};

// binary expression
class BinaryAST : public BaseAST {  // including: +=*/; || && ; LVal = ExpAST ; .....
public:
    enum class Operator {
        Add, Sub, Mul, Div, Mod,
        And, Or,  // logic
        Equal, NotEqual, Less, LessEq, Great, GreatEq,
        Assign
    };
    bool is_const;
    Operator op;
    ASTPtr left, right;
    string myID;// if const: it looks like int!
    int myValue;
    BinaryAST(Operator op, ASTPtr l, ASTPtr r, bool is_const):op(op),left(l),right(r),is_const(is_const)
    {

        if(is_const)
        {
            switch(op)
            {
                case Operator::Add:
                    myValue = ((ExpAST*)left)->value() + ((ExpAST*)right)->value();
                    break;
                case Operator::Sub:
                    myValue = ((ExpAST*)left)->value() - ((ExpAST*)right)->value();
                    break;
                case Operator::Mul:
                    myValue = ((ExpAST*)left)->value() * ((ExpAST*)right)->value();
                    break;
                case Operator::Div:
                    myValue = ((ExpAST*)left)->value() / ((ExpAST*)right)->value();
                    break;
                case Operator::Mod:
                    myValue = ((ExpAST*)left)->value() % ((ExpAST*)right)->value();
                    break;
                case Operator::And:
                    myValue = ((ExpAST*)left)->value() && ((ExpAST*)right)->value();
                    break;
                case Operator::Or:
                    myValue = ((ExpAST*)left)->value() || ((ExpAST*)right)->value();
                    break;
                case Operator::Equal:
                    myValue = ((ExpAST*)left)->value() == ((ExpAST*)right)->value();
                    break;
                case Operator::NotEqual:
                    myValue = ((ExpAST*)left)->value() != ((ExpAST*)right)->value();
                    break;
                case Operator::Less:
                    myValue = ((ExpAST*)left)->value() < ((ExpAST*)right)->value();
                    break;
                case Operator::LessEq:
                    myValue = ((ExpAST*)left)->value() <= ((ExpAST*)right)->value();
                    break;
                case Operator::Great:
                    myValue = ((ExpAST*)left)->value() > ((ExpAST*)right)->value();
                    break;
                case Operator::GreatEq:
                    myValue = ((ExpAST*)left)->value() >= ((ExpAST*)right)->value();
                    break;
                default: assert(0); break;  // error
            }
        }
        else if (op != Operator::Assign) myID = newTmpID();
    }

    string the_final_id_in_eeyone()  // meaningful only when not const
    {
        assert(!is_const); return myID;
    }
    int value(){ assert(is_const); return myValue; }  // meaningful only when const

    void print(ASTPtr currloop)override;
    void print_logic(ASTPtr currloop, const string& then_stmt, const string& else_stmt);

};

// unary expression
class UnaryAST : public BaseAST  // IntAST | ( Exp ) | FuncCallAST | UnaryOp UnaryAST | IdAST | IndexAST | return
{
public:
    enum class Operator{ Pos, Neg, Not };
    enum class Type {intAST, ExpBracket, funcCall, UnaryOp, IdAST, Index, returnAST};

    bool is_const;
    Operator op;
    ASTPtr ast;
    Type type;
    bool as_LVal;  // only meaningful when type = index.

    string myID;
    int myValue;

    UnaryAST(bool is_const, Operator op, ASTPtr ast,Type t, bool as_LVal);

    string the_final_id_in_eeyone()  // meaningful only when not const
    {
        assert(!is_const); return myID;
    }
    int value(){ assert(is_const); return myValue; }  // meaningful only when const

    void print(ASTPtr currloop)override;
    void print_logic(ASTPtr currloop,const string& then_stmt, const string& else_stmt);
};


// indexing
class IndexAST : public BaseAST {
    // main use: search in the array idlist and return info!
    // will not be used when array def
public:

    string id_in_sysY;
    string index_value_id_in_ir;
    vector<ExpAST*> dims;  // maybe init value; may be index value!
    bool is_const;

    IndexAST(string id_in_sysY, const vector<ExpAST*>& dim):id_in_sysY(id_in_sysY)
    {
        is_const = true;
        for(int i = 0; i < dim.size(); i++)
        {
            dims.emplace_back(dim[i]);
            if(! dim[i] -> is_const)is_const = false;
        }
        if(! find_info()->is_const) is_const = false;
    }

    arrayListElement* find_info()
    {
        for(int i = arrayList.size()-1;i>=0;i--)
        {
            if(arrayList[i].id_in_SysY == id_in_sysY)
            {
                return &(arrayList[i]);
            }
        }
        return nullptr;
    }

    string my_ident_only()
    {
        arrayListElement* info = find_info();
        return info->id_in_ir;

    }  // without [t1] info. just "T1".

    void print(ASTPtr currloop)override  // print calculating index process!
    {
        // need to print its calculating process!
        arrayListElement* info = find_info();


        int currTime = 1;
        string tmp;
        string code;

        index_value_id_in_ir = newTmpID();

        code = index_value_id_in_ir;  // tn = 0
        code += " = 0\n";
        otherCode.emplace_back(code);

        tmp = newTmpID();
        for(int i=dims.size()-1;i>=0;i--)
        {
            code = tmp;  // tmp = tx * currTime;
            code += " = ";

            dims[i]->print(currloop);
            if(dims[i]->is_const)
            {
                code += to_string(dims[i]->value());
            }
            else
            {
                //cout<<"dim_print start"<<endl;
                code += dims[i]->the_final_id_in_eeyone();
                //cout<<"dim_print end"<<endl;
            }

            code += " * ";
            code += to_string(currTime);
            code += "\n";
            otherCode.emplace_back(code);

            code = index_value_id_in_ir;  // tn = tn + tmp;
            code += " = ";
            code += index_value_id_in_ir;
            code += " + ";
            code += tmp;
            code += "\n";
            otherCode.emplace_back(code);

            currTime *= info->dims[i];
        }

        code = index_value_id_in_ir;  // tn = tn * 4
        code += " = ";
        code += index_value_id_in_ir;
        code += " * 4\n";
        otherCode.emplace_back(code);
    }

    string myID()  // without '\n' !
    {
        arrayListElement* info = find_info();
        string code = info->id_in_ir;
        code += "[";
        code += index_value_id_in_ir;
        code += "]";
        return code;
    }  // its shape in eeyone. e.g., T1[t1]. used in unaryAST!
    int value()  // when index_value is not const: the indexAST is not const !!!
    {
        assert(is_const);
        arrayListElement* info = find_info();
        assert(info != nullptr);
        int index_value=0;
        int currTime = 1;
        for(int i=dims.size()-1;i>=0;i--)
        {

            index_value += dims[i]->value() * currTime;
            currTime *= info->dims[i];
        }
        return info->value[index_value];
    }  // only meaningful if const

};

// integer number literal
class IntAST : public BaseAST {  // unsigned!
    //todo
public:
    int num;
    explicit IntAST(string value)
    {
        if(value[0]!='0')num = atoi(value.c_str());
        else
        {
            if(value[1]=='x' ||value[1]=='X')
            {
                num=hex2dec(value);
            }
            else{
                num=oct2dec(value);
            }
        }
    }
    int value(){return num;}
    string myvalue(){return to_string(num);}  // used for output
    void print(ASTPtr currloop)override{}  // never used
};

// identifier
class IdAST : public BaseAST {  // only used to keep var id.(array id: index; func id: funccall!)
    // main use: search in the var idlist and return info!
public:

    string id_in_SysY;  // to find its idlist pt
    explicit IdAST(string& id):id_in_SysY(id){}  // the idlist construction is done when def! not now!

    // the output id (Tx) info should be in idlist.

    // if it is const, its value should be initialized when def.
    // put its (initial) value in idlist
    // is_const info is known when def but do not know when use.

    // 变量名可以和函数名相同！

    pair<varListElement*,int> find_id_info()
    {
        for(int i=varList.size()-1;i>=0;i--)
        {
            if(varList[i].id_in_SysY == id_in_SysY)
            {
                return pair<varListElement*,int>(&(varList[i]),i);
            }
        }
        return pair<varListElement*,int>(nullptr,-1);
    }

    pair<arrayListElement*,int> find_array_info()
    {
        for(int i=arrayList.size()-1;i>=0;i--)
        {
            if(arrayList[i].id_in_SysY == id_in_SysY)
            {
                return pair<arrayListElement*,int>(&(arrayList[i]),i);
            }
        }
        return pair<arrayListElement*,int>(nullptr,-1);
    }

    pair<void*,bool> find_info()  // bool: 0: var; 1: array
    {
        pair<varListElement*,int> a = find_id_info();
        pair<arrayListElement*,int> b = find_array_info();
        if(!a.first)return pair<void*,bool>(b.first,1);
        if(!b.first)return pair<void*,bool>(a.first,0);
        for(int i = arrayBlockStart.size()-1; i >= 0; i++)
        {
            if(a.second > varBlockStart[i])return pair<void*,bool>(a.first,0);
            if(b.second > arrayBlockStart[i])return pair<void*,bool>(b.first,1);
        }
        // todo: if arrayBlockStart is empty (global var): this should not happen!(a and b are both valid pt and are both global var)
        assert(false);

    }

    bool const_or_not()
    {
        pair<void*,bool> idInfo=find_info();
        if(idInfo.second)return ((arrayListElement*)(idInfo.first))->is_const;
        return ((varListElement*)(idInfo.first))->is_const;
    }
    int value()
    {
        pair<void*,bool> idInfo=find_info();
        assert(!idInfo.second && ((varListElement*)(idInfo.first))->is_const);
        return ((varListElement*)(idInfo.first))->value;
    }  // only meaningful if const

    string id_in_sysy()
    {
        return id_in_SysY;
    }  // id in c ( used for index idlist initialization)
    string myId()
    {
        pair<void*,bool> idInfo=find_info();
        if(idInfo.second)return ((arrayListElement*)(idInfo.first))->id_in_ir;
        //cout<<"idast-myID()-find_info in varList\n";//debug
        return ((varListElement*)(idInfo.first))->id_in_ir;
    }  // id in ir

    void print(ASTPtr currloop)override {}  // never used!
};

typedef BinaryAST::Operator BinOp;
typedef UnaryAST::Operator UnaryOp;
typedef UnaryAST::Type UnaryType;



