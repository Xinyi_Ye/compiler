%{
#include<stdio.h>
#include<stdlib.h>
#include<ctype.h>
#include<string.h>
#include"source.h"
#include"source.tab.hpp"
using namespace std;
%}

digit           [0-9]
letter          [A-Za-z_]
hexadecimal_digit    [0-9a-fA-F]
octal_digit         [0-7]
nonzero_digit       [1-9]

id              ({letter})({letter}|{digit})*
dec       	((({nonzero_digit})({digit})*)) 
hex        	(((("0x")|("0X"))({hexadecimal_digit})+)) 
oct        	(("0")({octal_digit})*)
int_const    	{hex}|{oct}|{dec}

comment1         "//"[^\n]*
comment2         ("/*"([^\*]|(\*)*[^\*/])*(\*)*"*/")
comment          {comment1}|{comment2}

space 		[ \t]+
enter 		[\n]
space_enter     {space}|{enter}
%%

{space_enter}   {}
"int"           {return INT;}
"break"         {return BREAK;}
"else"          {return ELSE;}
"return"        {return RETURN;}
"const"         {return CONST;}
"continue"      {return CONTINUE;}
"void"          {return VOID;}
"if"            {return IF;}
"while"         {return WHILE;}
{int_const}     {string* str = new string(yytext); yylval.str = str; return INT_CONST;}
"+"			{return ADD;}
"-"			{return SUB;}
"*"			{return MUL;}
"/"			{return DIV;}
"%"			{return MOD;}

"="			{return ASG;}
"!"			{return NOT;}
"<"			{return LT;}
">"			{return GT;}
"<="        {return LE;}
">="        {return GE;}
"&&"		{return AND;}
"||"		{return OR;}
"=="		{return EQU;}
"!="		{return NEQ;}
"("			{return LBS;}
")"			{return RBS;}
"["			{return LBM;}
"]"			{return RBM;}
"{"			{return LBL;}
"}"			{return RBL;}
","			{return COM;}
";"			{return SEM;}



{id}        {string* str= new string(yytext); yylval.str = str; return IDENT;}
{comment}   {}

%%
#include <ctype.h>

int yywrap()
{
        return 1;
}
