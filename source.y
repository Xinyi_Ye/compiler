%{
    #include <ctype.h>
    #include<stdio.h>
    #include "source.h"
    #include <iostream>
    #include <vector>
    #include <cassert>
    #define MSDOS
    
    int yylex();
    int yyerror(char*);
    
    int tmp_id_cnt=0;
    int IRIDcnt=0;
    int paramcnt=0;
    int labelcnt=0;
    
    using namespace std;
    
    vector<varListElement> varList;
    vector<int> varBlockStart;
    vector<arrayListElement> arrayList;
    vector<int> arrayBlockStart;
    
    vector<pair<string,bool>> funcname_isvoid;
    
    vector<string> varDefCode;
    vector<string> otherCode;
    
    ASTPtr root;
    
    string bin_ops[14]={" + ", " - ", " * ", " / ", " % ", " && ", " || ",
        " == ", " != ", " < ", " <= ", " > ", " >= ", " = "};
    
    string unary_ops[3] = {"", " -", " !"};  // "+" can be omitted.
    
    
    %}

%union{
    ASTPtr pt;
    vector<ASTPtr> * ptList;
    vector<InitListAST*> * initList;
    vector<ExpAST*> * expList;
    bool b;
    UnaryOp unaryop;
    BinOp binop;
    std::string * str;
}

%type <pt> Root CompUnit Decl ConstDecl ConstDef ConstInitVal VarDef InitVal VarDecl FuncDef FuncFParam Block BlockItem Stmt Exp Cond LVal PrimaryExp Number Id UnaryExp MulExp AddExp RelExp EqExp LAndExp LOrExp ConstExp
%type <ptList> ConstDefList BMPairOrEmpty VarDefList FuncFParams BlockItems FuncRParams
%type <initList> InitValList ConstInitValList
%type <expList> BMPairOrEmptyNotConst
%type <b> FuncType
%type <unaryop> UnaryOp
%type <binop> MulOp AddOp RelOp EqOp
%type <str> INT_CONST IDENT

%token IF ELSE WHILE RETURN ADD SUB MUL DIV MOD CONTINUE BREAK
%token ASG NOT LT GT LE GE AND OR EQU NEQ LBS RBS LBM RBM LBL RBL COM SEM
%token CONST INT IDENT INT_CONST VOID



%%
Root:          CompUnit { $$ = $1; root = $1;//cout<<"root\n";
    
};

CompUnit:     CompUnit Decl         {$$ = new CompUnit($1,$2);//cout<<"compunit1\n";
    
}
| CompUnit FuncDef    {$$ = new CompUnit($1,$2);//cout<<"compunit2\n";
    
}
| Decl            {$$ = $1;//cout<<"compunit3\n";
    
}
| FuncDef        {$$ = $1;//cout<<"compunit4\n";
    
};

Decl:             ConstDecl        {$$ = $1;}
| VarDecl        {$$ = $1;//cout<<"decl-vardecl\n";
    
};

ConstDecl:        CONST FuncType ConstDef ConstDefList SEM
{
    ASTPtrList a;
    a.emplace_back($3);
    for(int i=0;i<$4->size();i++)
    {
        a.emplace_back((* $4)[i]);
    }
    $$ = new VarDeclAST(true,a);
};

ConstDef:         Id BMPairOrEmpty ASG ConstInitVal
{
    if($2->empty())
    {
        //vardef
        $$ = new VarDefAST(true,$1,((InitListAST*)$4)->expAST);
    }
    else
    {
        //arraydef
        $$ = new ArrayDefAST(true,$1,((InitListAST*)$4),* $2);
    }
};

ConstDefList:      /* epsilon */                {ASTPtrList* a = new ASTPtrList(); $$ = a;}
| ConstDefList COM ConstDef        {ASTPtrList* a = new ASTPtrList(* $1); a->emplace_back($3); $$ = a;};

BMPairOrEmpty:      /* epsilon */                {$$ = new ASTPtrList();//cout<<"bmpair-empty\n";
    
}
| BMPairOrEmpty LBM ConstExp RBM    {$$ = new ASTPtrList(* $1); $$->emplace_back($3);//cout<<"bmpair-not-empty\n";
    
};

ConstInitVal:     ConstExp                    {$$ = new InitListAST(vector<InitListAST*>(),$1);}
| LBL RBL                    {$$ = new InitListAST(vector<InitListAST*>(),nullptr);}
| LBL ConstInitVal ConstInitValList RBL
{
    vector<InitListAST*> a;
    a.emplace_back((InitListAST*)$2);
    for(int i=0;i<$3->size();i++)
    {
        a.emplace_back((* $3)[i]);
    }
    $$ = new InitListAST(a,nullptr);
};

ConstInitValList:       /* epsilon */                {$$ = new vector<InitListAST*> ();}
| ConstInitValList COM ConstInitVal    {$$ = new vector<InitListAST*> (* $1); $$->emplace_back((InitListAST*)$3);};




VarDecl:      FuncType VarDef VarDefList SEM
{
    ASTPtrList a;
    a.emplace_back($2);
    for(int i=0;i<$3->size();i++)
    {
        a.emplace_back((* $3)[i]);
    }
    $$ = new VarDeclAST(false,a);
};

VarDef:          Id BMPairOrEmpty
{
    if($2->empty())
    {
        //vardef
        $$ = new VarDefAST(false,$1,nullptr);
    }
    else
    {
        //arraydef
        $$ = new ArrayDefAST(false,$1,nullptr, * $2);
    }
}
| Id BMPairOrEmpty ASG InitVal
{
    if($2->empty())
    {
        //vardef
        $$ = new VarDefAST(false,$1,((InitListAST*)$4)->expAST);
    }
    else
    {
        //arraydef
        $$ = new ArrayDefAST(false,$1,((InitListAST*)$4), * $2);
    }
};

VarDefList:      /* epsilon */                {$$ = new ASTPtrList();}
| VarDefList COM VarDef         {$$ = new ASTPtrList(*$1); $$->emplace_back($3);};

InitVal:      Exp        { $$ = new InitListAST(vector<InitListAST*>(),$1);}
| LBL InitVal InitValList RBL
{
    vector<InitListAST*> a;
    a.emplace_back((InitListAST*)$2);
    for(int i=0;i<$3->size();i++)
    {
        a.emplace_back((* $3)[i]);
    }
    
    $$ = new InitListAST(a,nullptr);
}

| LBL RBL        {$$ = new InitListAST(vector<InitListAST*>(),nullptr);};





InitValList:       /* epsilon */        {$$ = new vector<InitListAST*> ();}
| InitValList COM InitVal        {$$ = new vector<InitListAST*>(*$1); $$->emplace_back((InitListAST*)$3);};








FuncDef:          FuncType Id LBS {pushBlockStart();} FuncFParams RBS Block        {$$ = new FuncDefAST($1,$2,* $5,$7); popBlockStart_and_pop_elements();//cout<<"funcdef-with-param\n";
    
}
| FuncType Id LBS RBS Block             {$$ = new FuncDefAST($1,$2,ASTPtrList(),$5);//cout<<"funcdef-without-param\n";
    
};

FuncType:         VOID                         {$$ = false;//cout<<"void\n";
    
}
| INT                          {$$ = true;//cout<<"int\n";
    
}

FuncFParams:      FuncFParam                    {$$ = new ASTPtrList(); $$->emplace_back($1);}
| FuncFParams COM FuncFParam             {$$ = new ASTPtrList(* $1); $$->emplace_back($3);}

FuncFParam:       FuncType Id LBM RBM BMPairOrEmpty            {$$ = new FuncParamAST(true,$2,* $5);}
| FuncType Id                    {$$ = new FuncParamAST(false,$2,ASTPtrList());}






Block:            LBL {pushBlockStart(); } BlockItems {popBlockStart_and_pop_elements();} RBL                 {$$ = new BlockAST(*$3);}

BlockItems:      /* epsilon */                    {$$ = new ASTPtrList();}
| BlockItems BlockItem                 {$$ = new ASTPtrList(*$1); $$->emplace_back($2);}

BlockItem:        Decl                        {$$ = $1;}
| Stmt                         {$$ = $1;}

Stmt:             LVal ASG Exp SEM                {ASTPtr a = new BinaryAST(BinOp(13),$1,$3,false); $$ = new ExpAST(false,true,a);}
| Exp SEM                    {$$ = $1;}
| SEM                        {$$ = nullptr;}
| Block                        {$$ = $1;}

| IF LBS Cond RBS Stmt ELSE Stmt        {$$ = new IfElseAST($3,$5,$7);}
| IF LBS Cond RBS Stmt                {$$ = new IfElseAST($3,$5);}
| WHILE LBS Cond RBS Stmt            {$$ = new WhileAST($3,$5);}
| BREAK SEM                    {$$ = new ControlAST(false);}
| CONTINUE SEM                    {$$ = new ControlAST(true);}
| RETURN Exp SEM                {ASTPtr a = new UnaryAST(false,UnaryOp(0),$2,UnaryType(6),false); $$ = new ExpAST(false,false,a);//cout<<"return sth\n";
    
}
| RETURN SEM                     {ASTPtr a = new UnaryAST(false,UnaryOp(0),nullptr,UnaryType(6),false); $$ = new ExpAST(false,false,a);//cout<<"return nothing\n";
    
}





/* all exp is expast. Binary/Unary should be inside it. */
Exp:              AddExp                    {$$ = $1;}

Cond:             LOrExp                     {$$ = $1;}

LVal:             Id BMPairOrEmptyNotConst             {
    ASTPtr idOrIndex;
    if($2->empty())
    {
        idOrIndex = $1;
        $$ = new UnaryAST(((IdAST*)idOrIndex)->const_or_not(), UnaryOp(0),idOrIndex,UnaryType(4),true);
    }
    else
    {
        idOrIndex = new IndexAST(((IdAST*)$1)->id_in_sysy(),* $2);
        $$ = new UnaryAST(((IndexAST*)idOrIndex)->is_const, UnaryOp(0),idOrIndex,UnaryType(5),true);
    }
}

BMPairOrEmptyNotConst:      /* epsilon */                {$$ = new vector<ExpAST*>();}
| BMPairOrEmptyNotConst LBM Exp RBM     {$$ = new vector<ExpAST*>(*$1); $$->emplace_back((ExpAST*)$3);}

PrimaryExp:       LBS Exp RBS                    {$$ = $2;}
| LVal                        {((UnaryAST*)$1)->as_LVal = false; $$ = new ExpAST(((UnaryAST*)$1)->is_const,false,$1);}
| Number                     {ASTPtr unary = new UnaryAST(true,UnaryOp(0),$1,UnaryType(0),false); $$ = new ExpAST(true, false, unary);}

Number:           INT_CONST                     {$$ = new IntAST(* $1);}

Id:          IDENT                        {$$ = new IdAST(* $1);}

UnaryExp:         PrimaryExp                    {$$ = $1;}
| Id LBS FuncRParams RBS            {
    FuncCallAST* tmp = new FuncCallAST(((IdAST*)$1)->id_in_SysY);
    for(int i=0;i<$3->size();i++)
    {
        if(((ExpAST*)((*$3)[i]))->is_binary)
        {
            tmp->add_param((*$3)[i],0);
        }
        else
        {
            UnaryAST* unary = (UnaryAST*)(((ExpAST*)((*$3)[i]))->ast);
            if(unary->type == UnaryType::Index)
            {
                tmp->add_param(unary->ast,1);
            }
            else if(unary->type == UnaryType::IdAST)
            {
                tmp->add_param(unary->ast,2);
            }
            else
            {
                tmp->add_param((* $3)[i],0);
            }
        }
    }
    ASTPtr unary = new UnaryAST(false, UnaryOp(0), (ASTPtr)tmp, UnaryType(2), false);
    $$ = new ExpAST(false, false, unary);
}
| Id LBS RBS                    {ASTPtr a = new FuncCallAST(((IdAST*)$1)->id_in_SysY); ASTPtr unary = new UnaryAST(false, UnaryOp(0), a, UnaryType(2), false); $$ = new ExpAST(false, false, unary);}
| UnaryOp UnaryExp                 {UnaryAST* unary = new UnaryAST(((ExpAST*)$2)->is_const,$1,$2,UnaryType(3),false); $$ = new ExpAST(unary->is_const, false, unary);}

UnaryOp:          ADD                        {$$ = UnaryOp(0);}
| SUB                        {$$ = UnaryOp(1);}
| NOT                         {$$ = UnaryOp(2);}

FuncRParams:      Exp                        {$$ = new ASTPtrList(); $$->emplace_back($1);}
| FuncRParams COM Exp                 {$$ = new ASTPtrList(*$1); $$->emplace_back($3);}

MulExp:           UnaryExp                    {$$ = $1;}
| MulExp MulOp UnaryExp             {{BinaryAST* a = new BinaryAST($2,$1,$3,((((ExpAST*)$1)->is_const)&&(((ExpAST*)$3)->is_const))); $$ = new ExpAST(a->is_const, true, a);}}

MulOp:          MUL                        {$$ = BinOp(2);}
| DIV                        {$$ = BinOp(3);}
| MOD                         {$$ = BinOp(4);}

AddExp:           MulExp                    {$$ = $1;}
| AddExp AddOp MulExp                {{BinaryAST* a = new BinaryAST($2,$1,$3,((((ExpAST*)$1)->is_const)&&(((ExpAST*)$3)->is_const))); $$ = new ExpAST(a->is_const, true, a);}}

AddOp:          ADD                        {$$ = BinOp(0);}
| SUB                        {$$ = BinOp(1);}

RelExp:           AddExp                    {$$ = $1;}
| RelExp RelOp AddExp                 {{BinaryAST* a = new BinaryAST($2,$1,$3,((((ExpAST*)$1)->is_const)&&(((ExpAST*)$3)->is_const))); $$ = new ExpAST(a->is_const, true, a);}}

RelOp:          LT                        {$$ = BinOp(9);}
| GT                        {$$ = BinOp(11);}
| LE                        {$$ = BinOp(10);}
| GE                        {$$ = BinOp(12);}

EqExp:            RelExp                    {$$ = $1;}
| EqExp EqOp RelExp                 {{BinaryAST* a = new BinaryAST($2,$1,$3,((((ExpAST*)$1)->is_const)&&(((ExpAST*)$3)->is_const))); $$ = new ExpAST(a->is_const, true, a);}}

EqOp:          EQU                        {$$ = BinOp(7);}
| NEQ                         {$$ = BinOp(8);}

LAndExp:          EqExp                        {$$ = $1;}
| LAndExp AND EqExp                {BinaryAST* a = new BinaryAST(BinOp(5),$1,$3,((((ExpAST*)$1)->is_const)&&(((ExpAST*)$3)->is_const))); $$ = new ExpAST(a->is_const, true, a);}

LOrExp:           LAndExp                    {$$ = $1;}
| LOrExp OR LAndExp                 {BinaryAST* a = new BinaryAST(BinOp(6),$1,$3,((((ExpAST*)$1)->is_const)&&(((ExpAST*)$3)->is_const))); $$ = new ExpAST(a->is_const, true, a);}

ConstExp:         AddExp                    {$$ = $1;}


%%

int yyerror(char* s)
{
    fprintf(stderr,"syntactic error:%s\n",s);
    return 0;
}

int main(int argc,char* argv[])
{
    
    
    freopen(argv[3],"r",stdin);
    freopen(argv[5],"w",stdout);
    
    rewind(stdin);
    
    yyparse();
    
    
    varList.clear();
    varBlockStart.clear();
    arrayList.clear();
    arrayBlockStart.clear();
    
    root->print(nullptr);
    return 0;
}





